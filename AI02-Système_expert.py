def main():
    while True:
        print("\nMenu Principal:\n")
        print("1-Lancer le moteur")
        print("2-Ajouter une règle (que valable pour cet appel!)")
        print("3-Quitter")
        print("4-Test")
        print("\n\n")

        choix = input("Choisissez une option (1-3): ")

        if choix == '1':
            moteur_avant()
        elif choix == '2':
            ajouter_regle()
        elif choix == '3':
            quitter()
        elif choix == '4':
            if test_regle(({ "couple": "non", "enfants": "oui" }, ("budget", "1"))) :
                print("yes")
            else :
                print("no")

if __name__ == "__main__":
    main()

import re

# Base de Faits (BF)
BF = {
    "tourisme": "oui",
    "danger": "oui",
    "enfants": "oui",
    "couple": "non"
}

# Base de Règles (BR)
BR = [
    ({"enfants": "oui" }, ("danger", "non")),
    ({"enfants": "oui" }, ("voyage_long", "non")),
    ({"enfants": "oui" }, ("distance_max", "10000")),
    ({ "danger": "oui" }, ("enfants", "non")),
    ({ "couple": "non", "enfants": "oui" }, ("budget", "1")),
    ({ "couple": "non", "enfants": "oui" }, ("budget", "2")),
    ({ "couple": "oui", "enfants": "non" }, ("budget", "3")),
    ({ "couple": "non", "enfants": "non" }, ("budget", "4")),
    ({ "activité": "randonnée/découverte_inédite" }, ("enfants", "non")),
    ({ "tourisme": "oui" }, ("danger", "non")),
    ({ "danger": "oui" }, ("activité", "découverte_inédite")),
    ({ "couple": "non", "enfants": "non" }, ("budget_dispo", "4"))
]

faits_num = {
    "budget" : [4, 3, 2, 1], "distance_max" : [300000, 20000, 15000, 10000, 5000, 1000]
}

# Liste de pays dans la Base de Règles
Pays = [
    "Californie", "NY", "Utah", "Italie", "UK", "Chili", "Brésil", "Turkie",
    "Russie", "Bahamas", "Australie", "Japon", "Thaïlande", "Maroc", "Canada",
    "Afrique du Sud", "Norvège", "Inde", "La Réunion", "Corée du Nord", "Lune",
    "Colombie Britannique", "Quebec"
]

regles_viables = BR

def quitter():
    print("Au revoir !")
    exit()

def verifier_doublons():
    fait_occurrences = {}
    # fait_occurrences est un dictionnaire de faits, auxquels on associe leur nombre d'occurences dans la BF
    for fait, valeur in BF.items():
        if fait in fait_occurrences and (valeur=="oui" or valeur=="non"):
        # On ne compte que les faits qui peuvent être Vrai ou Faux
            fait_occurrences[fait] += 1
        else:
            fait_occurrences[fait] = 1
    # On ne renvoie que les doublons, donc ceux donc le nb d'occurences > 1
    doublons = [fait for fait, occurrences in fait_occurrences.items() if occurrences > 1]

    return doublons

def pays_trouves():
    # Renvoie la liste des pays trouvés une fois la base de faits épuisée
    pays=[]
    for fait, valeur in BF.items():
        if fait in Pays:
            pays.append(fait[1])
    return pays

def ajouter_regle ():
    afficher_BR()
    print("Attention : Entrez au moins un fait avant de conclure la règle\n")
    # Une règle est une liste de deux éléments
    # Le premier est un dictionnaire contennant les premisses
    # Le second est le couple (conclusion, valeur)
    nouv_regle=[]
    choix=0
    while choix!=3 :
        print("1-Ajouter une prémisse dans la règle\n")
        print("2-Ajouter la conclusion de la règle\n")
        choix=int(input("Choix :"))
        i=0
        if choix == 1:
            fait=str(input("Entrez un fait : "))
            valeur=str(input("Entrez sa valeur : "))
            if valeur.isnumeric() or () :
                if fait not in faits_num :
                    ajouter_fait_num(fait)
                    (str(nouv_regle))[0][fait+str(i)]=valeur
                    i += 1
                else :
                    if valeur not in faits_num[fait]:
                        print(f"Attention, ce fait prend des valeurs numériques parmis {faits_num[fait]}. Essayez une autre valeur.")
            else :
                nouv_regle[0][fait+str(i)]=valeur
                i += 1
        elif choix == 2:
            if i==0 :
                print("Erreur : Entrez au moins un fait avant de conclure la règle")
            else :
                fait=str(input("Entrez le fait de conclusion : "))
                valeur=input("Entrez sa valeur : ")
                nouv_regle[1]=(fait, valeur)
                choix=3
    BR.append(nouv_regle)


def tri_regles():
    # On en prend que les règles dont le fait de conclusion n'est pas déjà connu
    # Initialement, règles_viables=BR
    for regle in regles_viables:
        for fait, valeur in BF.items():
            if fait != regle[1][0]:
                regles_viables.append(regle)

# Algorithme principal
def moteur_avant():
    fin = False
    nb_erreurs=0
    pays_trouves=[]
    debut=True
    while not fin:
        fait_trouve = False
        doublons = verifier_doublons()
        if nb_erreurs == 3 :
            # Au bout de trois appels de l'algorithme sans trouver de règle applicable à la BF, on arrête
            print("Pas de résultat! Désolé...")
            fin = True
        elif doublons:
            print(f"Il y a des doublons pour les faits suivants : {', '.join(doublons)}")
            print("On ne peut donc pas procéder, car deux faits sont incompatibles. Réessayez avec d'autres faits.")
            fin = True
        elif not regles_viables or (fait_trouve==False and debut==False) : # Si on a plus de règles applicables pour les faits de la BF (toutes les conclusions possibles sont dèjà connues)
            if not pays_trouves :
                print("Pas assez de données. Voici celles qu'on a déjà : ")
                afficher_BF()
                fin=int(input("Souhaitez-vous ajouter un fait, ou abandonner ? (0-Continuer, 1-Abandonner)"))
                if not fin :
                    nouv_fait=str(input("Entrez un nouveau fait pour affiner la recherche :"))
                    nouv_fait_valeur=str(input("Entrez sa valeur:"))
                    ajouter_fait(nouv_fait, nouv_fait_valeur)
                    nb_erreurs+=1
                    tri_regles()
            else :
                # Succès !
                affiche_pays_trouves(pays_trouves)
                fin=True
        else:
            # Algorithme boucle principale (il existe au moins une règle dont )
            fait_trouve=False # On remet à jour fait_trouvé pour le nouveau tour
            afficher_BF()
            for regle in BR:
            # Pour chaque règle
                for fait in BF.items():
                # Pour chaque fait connu
                    if fait in regle :
                    # Si le fait est dans une des règles
                        if test_regle():
                        # Si toutes les premisses de la règle sont bien dans la BF et si chacun de ces faits ont les bonnes valeurs
                            # On ajoute la conclusion de la règle à la base de faits
                            ajouter_fait(regle[1][0],regle[1][1])
                            regles_viables.remove(regle)
                            fait_trouve=True #Si aucune règle viable n'a été vérifiée pendant le tour, aucune règle viable n'est vérifiable avec notre BF, donc on ajoute un fait ou on arrête
            # Actualisation de regles_viables
            tri_regles()
        debut=False

def affiche_pays_trouves(pays_trouves):
    print("Pays trouvés pour les faits donnés:")
    for pays in pays_trouves:
        print(f"{pays}")

def afficher_BF():
    print("Base de Faits :")
    for fait, valeur in BF.items():
        print(f"Le fait {fait} a la valeur {valeur}")

def afficher_BR():
    for index, regle in enumerate(BR, start=1):
        print(f"/n Règle {index}:")
        faits=regle[0]
        conclusion=regle[1]
        # On affiche les faits
        print("Faits:")
        if not faits:
            print("Il n'y a aucun fait")
        else:
            for fait, valeur in faits.items():
                print(f" {fait}: {valeur}")
        print(f"=> {conclusion[0]} = {conclusion[1]}")

def ajouter_fait(fait,valeur):
    if fait == 'pays':
        pays_trouves.append(valeur)
    BF[fait]=valeur

def const_BF():
    fait= input("Entrez un fait ou 0 pour finir ")
    while fait:
        valeur = input(f"Entrez la valeur du fait '{fait}': ")
        ajouter_fait(fait,valeur)

def test_regle(regle):
    nb_faits_verifies=0
    for fait_regle in regle[0].keys(): # On parcourt chaque prémisse de la règle
        for fait_BF, valeur in BF.items(): # Pour chaque fait connu dans la BF
            if fait_BF == fait_regle : # Si le fait connu intervient dans la règle
                if fait_BF in faits_num: # Si le fait prend des valeurs numériques
                    if  (regle[0][fait_BF][0] == '>' or regle[0][fait_BF][0] == '<') : # Si on teste la valeur avec un signe (">300" ou "<300", au lieu de "300", qui implique "=300")
                        signe, nombre = extraire_signe_et_nombre(regle[0][fait_BF]) # On extrait le signe et la valeur numérique en elle-même
                        # On vérifie si la prémisse est vérifiée, selon le signe
                        if signe == '>':
                            if valeur <= nombre:
                                return False
                        else : # Si le signe est <
                            if valeur >= nombre:
                                return False
                    else : # Si la prémisse teste l'égalité
                        if valeur != int(regle[0][fait_BF]):
                            return False
                elif "/" in regle[0][fait_BF] : # Si le fait dans la règle propose au moins deux options de valeurs pour être vérifiée
                    options=extraire_options(regle[0][fait_BF])
                    if valeur not in options:
                        return False
                else : # Si le fait ne prend pas de valeurs numériques et n'a pas d'option de valeur "ou"
                    if valeur != regle[0][fait_BF]: # Si les deux valeurs du fait ne sont pas égales
                        return False
                nb_faits_verifies += 1
            # Si le fait connu n'intervient pas dans la règle, on le passe
    # Une fois toutes les prémisses parcourues, on compte le nombre de prémisses qu'on a vérifié avec les faits de la BF
    if nb_faits_verifies != len(regle[0]) :
        return False
    else :
        return True

def extraire_signe_et_nombre(chaine):
    # Utilisation d'une expression régulière pour extraire le signe et le nombre
    match = re.match(r'([<>]=?)\s*(\d+)', chaine)
    if match:
        signe = match.group(1)
        nombre = int(match.group(2))
        return signe, nombre
    else:
        return None, None

def extraire_options(chaine):
    # On sépare la chaîne en fonction du caractère "/"
    options = chaine.split("/")
    return options

def ajouter_fait_num(fait):
    fin = False
    faits_num.append(fait)
    print("Ce fait a une valeur numérique. Entrez les valeurs autorisées :")
    while choix!=3 :
        print("1-Ajouter une valeur")
        print("2-Quitter")
        choix=int(input("Choix :"))
        i=0
        if choix == 1:
            valeur=str(input("Entrez la valeur : "))
            if valeur.isnumeric():
                faits_num[fait].append(valeur)
                i += 1
            else :
                print("Erreur : la valeur doit être numérique")
        elif choix == 2:
            if i == 0 :
                print("Erreur : Entrez au moins une valeur numérique")
            else :
                print("Fait numérique ajouté. Continuez.")
                choix=3
